package tracker.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tracker.model.Budget;

import java.util.List;

@Repository
public interface BudgetRepository extends JpaRepository<Budget,Integer> {

    public List<Budget> findAllByIdIn(List<Integer> ids);
}
