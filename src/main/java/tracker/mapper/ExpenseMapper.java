package tracker.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tracker.dto.ExpenseDto;
import tracker.model.Expense;
import tracker.repo.BudgetRepository;
import tracker.repo.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExpenseMapper {

    @Autowired
    private  BudgetRepository budgetRepository;

    public  List<ExpenseDto> toDtoList(final List<Expense> expenses) {
        return expenses.stream().map(this::toDto).collect(Collectors.toList());
    }

    public  List<Expense> toEntityList(final List<ExpenseDto> expenses) {
        return expenses.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public  ExpenseDto toDto(final Expense expense) {
        return ExpenseDto.builder()
                .expenseId(expense.getId())
                .budgetId(expense.getBudget() == null ? null : expense.getBudget().getId())
                .expenseType(expense.getExpenseType())
                .value(expense.getValue())
                .build();
    }

    public  Expense toEntity(final ExpenseDto expenseDto) {
        return Expense.builder()
                .budget(budgetRepository.findById(expenseDto.getBudgetId()).orElse(null))
                .expenseType(expenseDto.getExpenseType())
                .value(expenseDto.getValue())
                .build();
    }


}
