package tracker.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tracker.dto.UserDto;
import tracker.model.Budget;
import tracker.model.User;
import tracker.repo.BudgetRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    @Autowired
    private BudgetRepository budgetRepository;

    public List<UserDto> toDtoList(final List<User> users) {
        return users.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<User> toEntityList(final List<UserDto> users) {
        return users.stream().map(this::toEntity).collect(Collectors.toList());

    }

    public UserDto toDto(final User user) {
        return UserDto.builder()
                .userId(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .currentIncome(user.getCurrentIncome())
                .budgetIds(user.getBudgets() == null || user.getBudgets().isEmpty() ? null : user.getBudgets().stream().map(Budget::getId).collect(Collectors.toList()))
                .email(user.getEmail())
                .userRole(user.getUserRole())
                .build();
    }

    public User toEntity(final UserDto userDto) {
        return User.builder()
                .budgets(userDto.getBudgetIds() == null || userDto.getBudgetIds().isEmpty() ? null : budgetRepository.findAllByIdIn(userDto.getBudgetIds()))
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .currentIncome(userDto.getCurrentIncome())
                .username(userDto.getUsername())
                .userRole(userDto.getUserRole())
                .build();
    }


}
