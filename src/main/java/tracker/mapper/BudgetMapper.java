package tracker.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tracker.dto.BudgetDto;
import tracker.model.Budget;
import tracker.model.Expense;
import tracker.repo.ExpenseRepository;
import tracker.repo.UserRepository;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class BudgetMapper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    public List<BudgetDto> toDtoList(List<Budget> budgets) {
        return budgets.stream().map(this::toDto).collect(Collectors.toList());
    }

    public List<Budget> toEntityList(List<BudgetDto> budgets) {
        return budgets.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public BudgetDto toDto(Budget budget) {
        return BudgetDto.builder()
                .budtedId(budget.getId())
                .income(budget.getIncome())
                .budgetName(budget.getName())
                .expensesIds(budget.getExpenses() == null || budget.getExpenses().isEmpty() ? null : budget.getExpenses().stream().map(Expense::getId).collect(Collectors.toList()))
                .userId(budget.getUser().getId())
                .build();
    }

    public Budget toEntity(BudgetDto budgetDto) {
        return Budget.builder()
                .income(budgetDto.getIncome())
                .name(budgetDto.getBudgetName())
                .expenses(budgetDto.getExpensesIds() == null || budgetDto.getExpensesIds().isEmpty() ? null : expenseRepository.findAllByIdIn(budgetDto.getExpensesIds()))
                .user(userRepository.findById(budgetDto.getUserId()).orElse(null))
                .build();

    }

}
