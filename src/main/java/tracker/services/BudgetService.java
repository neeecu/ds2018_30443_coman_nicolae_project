package tracker.services;

import tracker.dto.BudgetDto;
import tracker.model.Budget;

import java.util.Map;

public interface BudgetService {
    public BudgetDto makeBudget(final BudgetDto budget, final int userID);

    public boolean remove(final int budgetId, final int userId);

    public BudgetDto getBudget(final int budgetId);

    public BudgetDto updateBudget(final BudgetDto budget, final int budgetId);

    public Map<String, Double> getBudgetReport(int budgetId);
}
