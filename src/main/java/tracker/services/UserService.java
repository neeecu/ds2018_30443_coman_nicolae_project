package tracker.services;

import tracker.dto.UserDto;
import tracker.model.User;

import java.util.List;

public interface UserService {

    public UserDto save(final UserDto user);

    public UserDto findById(final int id);

    public UserDto update(final UserDto user, final int id);

    public boolean delete(final int id);

    public User findByUsername(String username);

    List<UserDto> getAll();
}

