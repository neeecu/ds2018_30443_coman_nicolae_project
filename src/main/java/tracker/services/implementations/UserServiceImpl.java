package tracker.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tracker.dto.UserDto;
import tracker.exception.customExceptions.WrongDataException;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.mapper.UserMapper;
import tracker.model.User;
import tracker.repo.UserRepository;

import org.springframework.transaction.annotation.Transactional;
import tracker.services.UserService;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDto save(final UserDto user) {
        checkUsernameAndEmail(user);
        if (!isGreterThanZero(user.getCurrentIncome())) {
            throw new WrongDataException(HttpStatus.NOT_ACCEPTABLE, "The user income must be positive", "Not allowed");
        }
        User entity = userMapper.toEntity(user);
        return userMapper.toDto(userRepository.save(entity));

    }

    @Override
    public UserDto findById(final int id) {
        if (!checkIfUserExists(id)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The user with the id: " + id + " does not exist", "Not Found");
        }
        return userMapper.toDto(userRepository.findById(id).get());
    }

    @Override
    public UserDto update(final UserDto user, final int id) {

        if (!checkIfUserExists(id)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The user with the id: " + id + " does not exist", "Not Found");
        }
        checkUsernameAndEmailForUpdate(user.getUsername(), user.getEmail(), id);
         /*get entity containing all budgets, copy them in the dto, convert back dto to entity, update
         I do this in because makeBudget/Budget methods should be used to alter budgets in a user, not update */
        User entity = userRepository.findById(id).get();
        copyBudgetIds(entity, user);
        copyAllButBudgets(user, entity);
        entity.setId(id);
        return userMapper.toDto(userRepository.save(entity));
    }


    private void copyBudgetIds(User from, UserDto to) {
        to.setBudgetIds(new ArrayList<>());
        from.getBudgets().stream().map(budget -> to.getBudgetIds().add(budget.getId()));
    }


    private void checkUsernameAndEmailForUpdate(final String username, final String email, final int id) {
        if (!userRepository.findAllByUsernameAndIdIsNot(username, id).isEmpty()) {
            throw new WrongDataException(HttpStatus.NOT_ACCEPTABLE, "Username already in use", "Not allowed");
        }

        if (!userRepository.findAllByEmailAndIdIsNot(email, id).isEmpty()) {
            throw new WrongDataException(HttpStatus.NOT_ACCEPTABLE, "Email already in use", "Not allowed");
        }

    }

    @Override
    public boolean delete(final int id) {
        User existentUser = userRepository.findById(id).orElse(null);
        if (existentUser == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The user with the id: " + id + " does not exist", "Not Found");
        }
        userRepository.delete(existentUser);
        return true;
    }

    @Override
    public User findByUsername(String username) {
        if (!checkIfUserExists(username)) {
            return null;
        }

        return userRepository.findByUsername(username);
    }

    @Override
    public List<UserDto> getAll() {
        return userMapper.toDtoList(userRepository.findAll());
    }


    private void checkUsernameAndEmail(final UserDto user) {
        if (userRepository.findByUsername(user.getUsername()) != null) {
            throw new WrongDataException(HttpStatus.NOT_ACCEPTABLE, "Username already in use", "Not Allowed");
        }

        if (userRepository.findByEmail(user.getEmail()) != null) {
            throw new WrongDataException(HttpStatus.NOT_ACCEPTABLE, "Email already in use", "Not Allowed");
        }
    }

    private boolean checkIfUserExists(final int id) {
        if (!userRepository.findById(id).isPresent()) {
            return false;
        }
        return true;
    }

    private boolean checkIfUserExists(final String username) {
        if (userRepository.findByUsername(username) == null) {
            return false;
        }
        return true;

    }

    private boolean isGreterThanZero(double currentIncome) {
        if (currentIncome <= 0) {
            return false;
        }
        return true;
    }

    private void copyAllButBudgets(UserDto from, User to) {
        to.setCurrentIncome(from.getCurrentIncome());
        to.setEmail(from.getEmail());
        to.setPassword(from.getPassword());
        to.setUsername(from.getUsername());
        to.setUserRole(from.getUserRole());
    }

}
