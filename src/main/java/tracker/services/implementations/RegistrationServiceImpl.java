package tracker.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tracker.dto.TokenDto;
import tracker.dto.UserDto;
import tracker.exception.customExceptions.IllegalStateException;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.exception.customExceptions.WrongDataException;
import tracker.mapper.TokenMapper;
import tracker.model.Token;
import tracker.model.UserRole;
import tracker.repo.TokenRepository;
import tracker.services.RegistrationService;
import tracker.services.UserService;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private MailService mailService;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenMapper tokenMapper;

    private final String URL_STRING = "http://localhost:4200/register";

    @Override
    public TokenDto sendToken(String destEmail, boolean isAdmin) {
        String token = UUID.randomUUID().toString();
        boolean isSent = mailService.sendMail("mockingemailaddress@yahoo.com", "!abcd1234"
                , destEmail, "Registration Token", "In order to register, please go to:\n"
                        + URL_STRING + "\n and use " + token + " as token");
        if (isSent) {
            Token toSave = Token.builder()
                    .consumed(false)
                    .admin(isAdmin)
                    .email(destEmail)
                    .token(token)
                    .build();
            tokenRepository.save(toSave);
            return tokenMapper.toDto(toSave);
        } else {
            throw new IllegalStateException(HttpStatus.UNPROCESSABLE_ENTITY, "The email could not be sent", "Unprocessable entity");
        }

    }

    @Override
    public UserDto register(UserDto userDto, String token) {
        Token dbToken = tokenRepository.findByToken(token);
        validateToken(dbToken, userDto.getEmail());
        if (dbToken.isAdmin()) {
            userDto.setUserRole(UserRole.ADMIN);
        } else {
            userDto.setUserRole(UserRole.USER);
        }
        tokenRepository.delete(dbToken);
        return userService.save(userDto);
    }

    private void validateToken(Token dBToken, String email) {
        Token emailEntry = tokenRepository.findByEmail(email);
        //the token is wrong, no entry in the db for the code provided by the user in the web page
        if (dBToken == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The provided token is wrong, please enter the " +
                    "token received by email carefully", "Not found");
        } else {
            if (emailEntry == null) {
                throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The email provided does not hold any token "
                        , "Not found");
            } else {
                if (!(dBToken.getEmail().equals(email))) {
                    throw new WrongDataException(HttpStatus.FORBIDDEN, "Please use the email on which you received the token"
                            , "Forbidden");
                }
            }
        }

    }
}
