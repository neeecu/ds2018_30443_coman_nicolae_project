package tracker.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tracker.dto.BudgetDto;
import tracker.dto.ExpenseDto;
import tracker.exception.customExceptions.IllegalStateException;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.mapper.BudgetMapper;
import tracker.mapper.ExpenseMapper;
import tracker.model.Budget;
import tracker.model.Expense;
import tracker.repo.BudgetRepository;
import tracker.repo.ExpenseRepository;
import tracker.services.BudgetService;
import tracker.services.ExpenseService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExpensesServiceImpl implements ExpenseService {

    /* the mapper doesn't work so i do the mappings here */
    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private BudgetService budgetService;

    @Autowired
    private BudgetMapper budgetMapper;

    @Autowired
    private ExpenseMapper expenseMapper;

    @Override
    public ExpenseDto addExpense(final ExpenseDto expense, final int budgetId) {
        if (!doesBudgetExist(budgetId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The budget with the given id does not exist", "Not found");
        }
        expense.setBudgetId(budgetId);
        updateBudgetIncome(budgetId, expense.getValue());
        Expense toSave = expenseMapper.toEntity(expense);
        return expenseMapper.toDto(expenseRepository.save(toSave));
    }

    private void updateBudgetIncome(final int budgetId, final double expenseValue) {
        Budget budget = budgetRepository.findById(budgetId).get();
        budget.setIncome(budget.getIncome() - expenseValue);
        budgetService.updateBudget(budgetMapper.toDto(budget), budgetId);
    }

    @Override
    public void deleteExpense(final int expenseId, final int budgetId) {
        if (!doesExpenseExist(expenseId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The expense with the given id does not exist", "Not found");
        }
        updateBudgetIncomeWhenExpenseDeleted(expenseId, budgetId);
        expenseRepository.deleteById(expenseId);

    }

    private void updateBudgetIncomeWhenExpenseDeleted(int expenseId, int budgetId) {
        Budget budget = budgetRepository.findById(budgetId).get();
        Expense expense = expenseRepository.findById(expenseId);
        budget.setIncome(budget.getIncome() + expense.getValue());
        budgetService.updateBudget(budgetMapper.toDto(budget), budgetId);
    }

    @Override
    public ExpenseDto updateExpense(final ExpenseDto expense, final int expenseId) {
        if (!doesExpenseExist(expenseId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The expense with the given id does not exist", "Not found");
        }
        if (!doesBudgetExist(expense.getBudgetId())) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The budget with the given id does not exist", "Not found");

        }
        if (expense.getValue() < 0) {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "The expense value cannot be negative. ", "Forbidden");
        }

        Expense entity = expenseRepository.findById(expenseId);
        expense.setExpenseId(entity.getId());
        double oldExpenseValue = entity.getValue();
        copyAllButBudget(expense, entity);
        updateBudgetWhenExpenseChanged(expense.getValue(), oldExpenseValue, expense.getBudgetId());
        return expenseMapper.toDto(expenseRepository.save(entity));
    }

    private void copyAllButBudget(ExpenseDto from, Expense to) {
        to.setValue(from.getValue());
        to.setExpenseType(from.getExpenseType());

    }

    private void updateBudgetWhenExpenseChanged(double newExpenseValue, double oldExpenseValue, final int budgetId) {
        Budget budget = budgetRepository.findById(budgetId).get();
        if (budget.getIncome() + oldExpenseValue - newExpenseValue >= 0) {
            budget.setIncome(budget.getIncome() + oldExpenseValue - newExpenseValue);
            budgetService.updateBudget(budgetMapper.toDto(budget), budget.getId());

        } else {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "The budget cannot be negative. ", "Forbidden");
        }

    }

    @Override
    public ExpenseDto findExpense(int expenseId) {
        if (!doesExpenseExist(expenseId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The expense with the given id does not exist", "Not found");
        }
        return expenseMapper.toDto(expenseRepository.findById(expenseId));
    }

    @Override
    public List<ExpenseDto> getAllExpenses(final int budgetId) {
        BudgetDto budgetDto = budgetService.getBudget(budgetId);
        return expenseMapper.toDtoList(expenseRepository.findAllByIdIn(budgetDto.getExpensesIds()));

    }

    private boolean doesBudgetExist(int budgetId) {
        if (!budgetRepository.findById(budgetId).isPresent()) {
            return false;
        }
        return true;
    }

    private boolean doesExpenseExist(int expenseId) {
        if (expenseRepository.findById(expenseId) == null) {
            return false;
        }
        return true;
    }

}
