package tracker.services;

import tracker.dto.ExpenseDto;
import tracker.model.Expense;

import java.util.List;

public interface ExpenseService {

    public ExpenseDto addExpense(final ExpenseDto expense, final int budgetId);

    public void deleteExpense(final int expenseId, final int budgetId);

    public ExpenseDto updateExpense(final ExpenseDto expense, final int expenseId);

    public ExpenseDto findExpense(final int expenseId);

    public List<ExpenseDto> getAllExpenses(final int budgetId);

}
