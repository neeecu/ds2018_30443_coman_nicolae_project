package tracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import tracker.dto.UserDto;
import tracker.security.JwtUtil;
import tracker.security.UserDetailsServiceImpl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class LogInController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    /*
    @Value("${jwt.header}")
    private String tokenHeader;
    */

    /* Logout simply made in front by deleteing the token from file system
    @GetMapping("/logout")
    public void doLogout(HttpServletRequest request, HttpServletResponse response) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
    }
    */

    @PostMapping("/authenticate")
    public Map doLogin(@RequestBody UserDto userDto)  throws AuthenticationException {
        authenticate(userDto.getUsername(), userDto.getPassword());

        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(userDto.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);

        // Return the token as json object
        Map<String,String> toReturn = new HashMap<>();
        toReturn.put("token", token);
        toReturn.put("role",getRole(userDetails.getAuthorities()));
        return toReturn;
    }

    /* not used for now, may never be used since front stores token in file system
    @RequestMapping(value = "/getJwtToken", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        return ResponseEntity.ok(token);

    }
    */

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new RestClientException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new RestClientException("Bad credentials!", e);
        }
    }

    private String getRole(Collection<? extends GrantedAuthority> autorities){
        return autorities.toArray()[0].toString();
    }
}
