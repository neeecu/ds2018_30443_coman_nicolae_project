package tracker.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tracker.dto.UserDto;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.services.BudgetService;
import tracker.services.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController()
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private BudgetService budgetService;


    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(httpMethod = "GET", value = "Get all users")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return new ResponseEntity<List<UserDto>>(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}/get")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "GET",
            value = "Find an user by id",
            response = UserDto.class
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public UserDto getUser(@NotNull @PathVariable
                           @ApiParam(value = "the id of the user to be found", required = true) final int id) {
        UserDto user = userService.findById(id);
        return user;
    }

    @PostMapping(value = "/save", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "POST",
            value = "UserDto",
            response = UserDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Created"),
                    @ApiResponse(code = 406, message = "User with given username/password already exists"),
                    @ApiResponse(code = 422, message = "Unprocessable entity")
            })
    public UserDto saveUser(@RequestBody @Valid @ApiParam("dto of the user to be saved") final UserDto user) {
        return userService.save(user);

    }


    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Update an User",
            response = UserDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Updated")}
    )

    public UserDto update(@NotNull @PathVariable @ApiParam("the id of the user to be updated") final int id,
                          @RequestBody @Valid @ApiParam("Dto with updated fields") final UserDto user) {
        return userService.update(user, id);

    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "DELETE",
            value = "Delete an user"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Deleted"),
                    @ApiResponse(code = 404, message = "NotFound")
            })
    public ResponseEntity<String> delete(@NotNull @PathVariable @ApiParam("the id of the user to be deleted") int id) {
        if (userService.delete(id)) {
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "User with id = " + id + " not found", "Not Found");
    }

}

