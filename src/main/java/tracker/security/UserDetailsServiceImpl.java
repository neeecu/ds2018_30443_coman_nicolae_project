package tracker.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import tracker.model.User;
import tracker.repo.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User dbUser = userRepository.findByUsername(username);
        if (dbUser == null) {
            throw new UsernameNotFoundException("No user found with username" + username);
        }

        return org.springframework.security.core.userdetails.User.withUsername(username)
                .passwordEncoder(encoder::encode)
                .password(dbUser.getPassword())
                .roles(dbUser.getUserRole().toString())
                .build();
    }


}
