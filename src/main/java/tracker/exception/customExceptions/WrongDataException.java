package tracker.exception.customExceptions;

import org.springframework.http.HttpStatus;

import java.util.List;

public class WrongDataException extends TrackerException {
    public WrongDataException(HttpStatus status, String message, List<String> errors) {
        super(status, message, errors);
    }

    public WrongDataException(HttpStatus status, String message, String error) {
        super(status, message, error);
    }
}
