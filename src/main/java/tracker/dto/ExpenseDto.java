package tracker.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tracker.model.Budget;
import tracker.model.ExpenseType;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class ExpenseDto {

    private int expenseId;

    @NotNull
    private int budgetId;

    private ExpenseType expenseType;

    private double value;

    @Builder
    public ExpenseDto(final int expenseId, final int budgetId, final ExpenseType expenseType, final double value) {
        this.expenseId = expenseId;
        this.budgetId = budgetId;
        this.expenseType = expenseType;
        this.value = value;
    }
}
