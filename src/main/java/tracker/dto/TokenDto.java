package tracker.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class TokenDto {

    private int id;

    @NotBlank
    private String token;

    @NotBlank
    @Email
    private String email;

    private boolean consumed;
    private boolean admin;

    @Builder
    public TokenDto(int id, @NotNull String token, @NotBlank @Email String email, boolean consumed
                    , boolean admin) {
        this.id = id;
        this.token = token;
        this.email = email;
        this.consumed = consumed;
        this.admin=admin;
    }
}
