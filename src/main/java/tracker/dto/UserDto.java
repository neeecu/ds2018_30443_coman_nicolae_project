package tracker.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import tracker.model.UserRole;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class UserDto {

    private int userId;

    @NotBlank
    @Size(min = 6)
    private String username;

    @NotBlank
    @Size(min = 6)
    private String password;

    @Email
    private String email;

    @NotNull
    private double currentIncome;

    private UserRole userRole;

    private List<Integer> budgetIds;

    @Builder
    public  UserDto(final int userId, final String username, final String password,
                   final String email, final double currentIncome, final UserRole userRole, final List<Integer> budgetIds) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.email = email;
        this.currentIncome = currentIncome;
        this.userRole = userRole;
        this.budgetIds = budgetIds;
    }

}

