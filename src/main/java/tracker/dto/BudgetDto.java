package tracker.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class BudgetDto {

    private int budgedId;

    private double income;

    private String budgetName;

    @NotNull
    private int userId;

    private List<Integer> expensesIds;

    @Builder
    public BudgetDto(final int budtedId, final double income, final String budgetName, final int userId, final List<Integer> expensesIds) {
        this.budgedId = budtedId;
        this.income = income;
        this.budgetName = budgetName;
        this.userId = userId;
        this.expensesIds = expensesIds;
    }
}
