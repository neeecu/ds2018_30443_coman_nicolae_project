package tracker.model;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "tokens")
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String token;
    private boolean consumed;
    private boolean admin;
    private String email;

    @Builder
    public Token(String token, boolean consumed, boolean admin, String email) {
        this.token = token;
        this.consumed = consumed;
        this.admin = admin;
        this.email = email;
    }
}
