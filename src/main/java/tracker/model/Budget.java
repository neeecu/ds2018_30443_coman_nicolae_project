package tracker.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "budgets")
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private double income;

    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_budget")
    private User user;

    @OneToMany(mappedBy = "budget", orphanRemoval = true)
    private List<Expense> expenses;

    @Builder
    public Budget(final double income, final String name, final User user, final List<Expense> expenses) {
        this.income = income;
        this.user = user;
        this.name = name;
        this.expenses = expenses;
    }
}
