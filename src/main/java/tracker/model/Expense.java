package tracker.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "expenses")
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne()
    @JoinColumn(name = "fk_expense")
    private Budget budget;

    private ExpenseType expenseType;

    private double value;

    @Builder
    public Expense(final Budget budget, final ExpenseType expenseType, final double value) {
        this.budget = budget;
        this.expenseType = expenseType;
        this.value = value;
    }
}
