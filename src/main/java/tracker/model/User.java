package tracker.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String username;

    private String password;

    private String email;

    private double currentIncome;

    @Enumerated(EnumType.STRING)
    UserRole userRole;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Budget> budgets;

    @Builder
    public User(final String username, final String password, final String email, final double currentIncome,
                final UserRole userRole, final List<Budget> budgets) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.currentIncome = currentIncome;
        this.userRole = userRole;
        this.budgets = budgets;
    }
}
