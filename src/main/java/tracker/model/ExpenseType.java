package tracker.model;

public enum ExpenseType {
    FOOD,
    ENTERTAINMENT,
    BILLS,
    FINES
}
